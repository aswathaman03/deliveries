package com.ash.deliveries.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ash.deliveries.datasource.network.Resource
import com.ash.deliveries.model.Delivery
import com.ash.deliveries.model.DeliveryRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel @Inject constructor(private val deliveryRepository: DeliveryRepository) : ViewModel() {

    val resource = MutableLiveData<Resource<List<Delivery>>>()
    val pagedListLiveData: LiveData<PagedList<Delivery>> by lazy {
        val dataSourceFactory = deliveryRepository.getSelectedPage()
        val config = PagedList.Config.Builder()
            .setPageSize(10)
            .setEnablePlaceholders(false)
            .build()
        LivePagedListBuilder(dataSourceFactory, config).build()
    }

    private val compositeDisposable = CompositeDisposable()


    fun loadDeliveries(page: Int) {
        resource.value = Resource.loading(resource.value?.data)
        with(deliveryRepository) {
            getDeliveries((page*10).toString(), "10").flatMap { it ->
                deliveryRepository.updateDeliveries(it, page == 0)
                Single.just(it)
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { disposable -> onSubscribed(disposable) }
                .subscribe({ value ->
                    resource.value = Resource.success(value)
                }, {
                    resource.value = Resource.error(null, it.message)
                })
        }

    }


    private fun onSubscribed(subscription: Disposable) {
        compositeDisposable.add(subscription)
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
