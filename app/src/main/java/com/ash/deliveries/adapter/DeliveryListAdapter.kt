package com.ash.deliveries.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ash.deliveries.R
import com.ash.deliveries.model.Delivery
import com.ash.deliveries.ui.ListFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.delivery_item.view.*


class DeliveryListAdapter : PagedListAdapter<Delivery, DeliveryListAdapter.ViewHolder>(DiffCallBack()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val delivery = getItem(position)
        holder.apply {
            bind(createOnClickListener(delivery!!), delivery)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.delivery_item, parent, false)
        return ViewHolder(view)
    }

    private fun createOnClickListener(delivery: Delivery): View.OnClickListener {
        return View.OnClickListener {
            val direction = ListFragmentDirections.actionListFragmentToDetailFragment(delivery)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bind(listener: View.OnClickListener, delivery: Delivery) {
            view.setOnClickListener(listener)
            with(delivery) {
                view.deliveryName.text = description
                Picasso.get()
                    .load(imageUrl)
                    .fit().centerCrop()
                    .error(R.drawable.ic_error_24dp)
                    .into(view.deliveryImage)
            }
        }
    }

}

private class DiffCallBack : DiffUtil.ItemCallback<Delivery>() {

    override fun areItemsTheSame(oldItem: Delivery, newItem: Delivery): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Delivery, newItem: Delivery): Boolean {
        return oldItem == newItem
    }
}

