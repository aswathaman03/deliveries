package com.ash.deliveries.datasource.db

import androidx.room.TypeConverter
import com.ash.deliveries.model.Location
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class LocationConverters {

    @TypeConverter
    fun locationToString(location: Location): String {
        val gson = Gson()
        val type = object : TypeToken<Location>() {}.type
        return gson.toJson(location, type)
    }

    @TypeConverter
    fun stringToLocation(locationString: String): Location {
        val type = object : TypeToken<Location>() {}.type
        return Gson().fromJson(locationString, type)
    }

}