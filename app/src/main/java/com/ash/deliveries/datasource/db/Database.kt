package com.ash.deliveries.datasource.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ash.deliveries.model.DeliveryDao
import com.ash.deliveries.model.Delivery


@Database(entities = arrayOf(Delivery::class), exportSchema = false, version = 1)
@TypeConverters(LocationConverters::class)
abstract class Database : RoomDatabase() {
    abstract fun deliveryDao(): DeliveryDao
}