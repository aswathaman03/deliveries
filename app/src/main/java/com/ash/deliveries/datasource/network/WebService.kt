package com.ash.deliveries.datasource.network

import com.ash.deliveries.model.Delivery
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {
    @GET("/deliveries")
    fun getDeliveries(@Query("offset") start: String, @Query("limit") limit: String): Single<List<Delivery>>
}

