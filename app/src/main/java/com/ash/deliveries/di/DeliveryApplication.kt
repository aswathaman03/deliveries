package com.ash.deliveries.di

import android.app.Application
import androidx.fragment.app.Fragment
import com.ash.deliveries.di.component.DaggerAppComponent
import com.ash.deliveries.di.module.AppModule
import com.ash.deliveries.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class DeliveryApplication : Application(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
            .builder()
            .applicationModule(AppModule(this))
            .networkModule(NetworkModule)
            .build()
            .inject(this)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>  = fragmentInjector
}