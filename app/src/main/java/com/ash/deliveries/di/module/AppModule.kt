package com.ash.deliveries.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.ash.deliveries.datasource.db.Database
import com.ash.deliveries.model.DeliveryDao
import com.ash.deliveries.util.GeneralUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {


    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideGeneralUtils(): GeneralUtils = GeneralUtils(app.applicationContext)

    @Provides
    @Singleton
    fun provideDatabase(app: Application): Database =
        Room.databaseBuilder(app, Database::class.java, "DELIVERY_DB").build()

    @Provides
    @Singleton
    fun provideDeliveryDao(db: Database): DeliveryDao = db.deliveryDao()

}