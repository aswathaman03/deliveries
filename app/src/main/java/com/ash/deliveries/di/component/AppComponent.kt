package com.ash.deliveries.di.component

import com.ash.deliveries.di.DeliveryApplication
import com.ash.deliveries.di.module.AppModule
import com.ash.deliveries.di.module.BuildersModule
import com.ash.deliveries.di.module.NetworkModule
import com.ash.deliveries.di.module.ViewModelModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(BuildersModule::class, ViewModelModule::class, AppModule::class, NetworkModule::class))
interface AppComponent {

    fun inject(app: DeliveryApplication)

    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        fun applicationModule(appModule: AppModule): Builder
        fun networkModule(netwrkModule: NetworkModule): Builder
    }

}