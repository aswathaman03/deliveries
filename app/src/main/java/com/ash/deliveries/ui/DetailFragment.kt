package com.ash.deliveries.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.ash.deliveries.R
import com.ash.deliveries.model.Delivery
import com.ash.deliveries.viewmodel.DetailViewModel
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.OnMapReadyCallback
import com.google.android.libraries.maps.SupportMapFragment
import com.google.android.libraries.maps.model.BitmapDescriptorFactory
import com.google.android.libraries.maps.model.LatLng
import com.google.android.libraries.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject


private const val DELIVERY = "Delivery"


class DetailFragment : Fragment(), OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val detailViewModel: DetailViewModel by viewModels {
        viewModelFactory
    }

    private var delivery: Delivery? = null
    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            delivery = it.getParcelable<Delivery>(DELIVERY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        AndroidSupportInjection.inject(this)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateView(delivery!!)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        addMarker(delivery!!)
    }

    private fun populateView(delivery: Delivery) {
        with(delivery) {
            deliveryName.text = description
            Picasso.get()
                .load(imageUrl)
                .fit().centerCrop()
                .error(R.drawable.ic_error_24dp)
                .into(deliveryImage)
        }
    }

    private fun addMarker(delivery: Delivery) {
        with(delivery) {
            val point = LatLng(location.lat, location.lng)
            val markerOptions = MarkerOptions().position(point)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin))
            map!!.addMarker(markerOptions)
            map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 14f))
        }
    }

}
