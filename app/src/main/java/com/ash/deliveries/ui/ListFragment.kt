package com.ash.deliveries.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ash.deliveries.R
import com.ash.deliveries.adapter.DeliveryListAdapter
import com.ash.deliveries.util.GeneralUtils
import com.ash.deliveries.util.ResourceObserver
import com.ash.deliveries.viewmodel.ListViewModel
import com.google.android.material.snackbar.Snackbar
import com.paginate.Paginate
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class ListFragment : Fragment(), Paginate.Callbacks {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var generalUtils: GeneralUtils
    private val listViewModel: ListViewModel by viewModels {
        viewModelFactory
    }

    private var snackbar: Snackbar? = null
    private var page = 0
    private var isLoading = false
    private var hasLoadedAllItems = false
    private var noInternetError = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        AndroidSupportInjection.inject(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe()
    }

    private fun subscribe() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter = DeliveryListAdapter()
        recyclerView.adapter = adapter
        Paginate.with(recyclerView, this).build()

        listViewModel.pagedListLiveData.observe(this, Observer {
            adapter.submitList(it)
        })

        listViewModel.resource.observe(
            this,
            ResourceObserver(progress = ::progress, fullyLoaded = ::fullyLoaded, onError = ::onError)
        )
    }

    override fun onLoadMore() {
        if (snackbar != null && generalUtils.hasConnection()) snackbar = null
        if (!isLoading) {
            isLoading = true
            noInternetError = false
            listViewModel.loadDeliveries(page++)
        }
    }

    override fun isLoading(): Boolean = isLoading

    override fun hasLoadedAllItems(): Boolean = hasLoadedAllItems

    private fun noInternet() {
        if (snackbar == null) {
            snackbar =
                Snackbar.make(container, requireContext().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                    .setAction(requireContext().getString(R.string.retry)) {
                        onLoadMore()
                        snackbar = null
                    }.also { it.show() }
        }
    }

    private fun onError(message: String) {
        page--
        if (!generalUtils.hasConnection()) {
            noInternetError = true
            noInternet()
        }
    }

    private fun progress(isLoading: Boolean) {
        this.isLoading = isLoading
    }

    private fun fullyLoaded(isFullyLoaded: Boolean) {
        this.hasLoadedAllItems = isFullyLoaded
        this.isLoading = false
    }

    override fun onResume() {
        super.onResume()
        if (noInternetError)
            onLoadMore()
    }

}
