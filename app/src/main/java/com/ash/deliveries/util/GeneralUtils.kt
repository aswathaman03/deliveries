package com.ash.deliveries.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo



class GeneralUtils constructor(private val context: Context) {

    fun hasConnection(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }

}