package com.ash.deliveries.util

import android.util.Log
import androidx.lifecycle.Observer
import com.ash.deliveries.datasource.network.Resource
import com.ash.deliveries.model.Delivery

class ResourceObserver<T>(
    private val progress: (isLoading: Boolean) -> Unit,
    private val fullyLoaded: (isFullyLoaded: Boolean) -> Unit,
    private val onError: (message: String) -> Unit

) : Observer<Resource<T>> {

    override fun onChanged(resource: Resource<T>?) {
        when (resource?.status) {

            Resource.Status.SUCCESS -> {
                progress(false)
                if ((resource.data as List<*>).size == 0) {
                    fullyLoaded(true)
                }
            }

            Resource.Status.ERROR -> {
                progress(false)
                if (resource.error != null) {
                    if (resource.data!=null && (resource.data as List<*>).size == 0) {
                        fullyLoaded(true)
                    }else{
                        onError(resource.error)
                    }
                }
            }

            Resource.Status.LOADING -> {
                progress(true)
            }

        }
    }
}