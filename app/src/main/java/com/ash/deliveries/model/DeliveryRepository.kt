package com.ash.deliveries.model


import com.ash.deliveries.datasource.network.WebService

import javax.inject.Inject

open class DeliveryRepository @Inject constructor(val deliveryDao: DeliveryDao, val webService: WebService) {

    fun getDeliveries(start: String, limit: String) = webService.getDeliveries(start, limit)

    fun getSelectedPage() = deliveryDao.selectPaged()

    fun updateDeliveries(deliveries: List<Delivery>, isFirstPage: Boolean) =
        deliveryDao.updateDeliveries(deliveries, isFirstPage)

}
