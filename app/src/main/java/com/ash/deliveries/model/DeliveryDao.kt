package com.ash.deliveries.model

import android.util.Log
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface DeliveryDao {

    @Query("SELECT * FROM delivery ORDER BY id ASC")
    fun selectPaged(): DataSource.Factory<Int, Delivery>

    @Transaction
    fun updateDeliveries(
        deliveries: List<Delivery>,
        isFirstPage: Boolean
    ) {
            val minId = (deliveries.first().id)
            val maxId = (deliveries.last().id)
            if (!isFirstPage) {
                deleteRange(minId, maxId)
            } else {
                deleteRangeFirst(minId)
            }
            insert(deliveries)
    }

    @Query("DELETE FROM delivery WHERE id BETWEEN :minId AND :maxId")
    fun deleteRange(minId: Int, maxId: Int)

    @Query("DELETE FROM delivery WHERE id < :minId")
    fun deleteRangeFirst(minId: Int)

    @Insert(onConflict = REPLACE)
    fun insert(deliveries: List<Delivery>)

}


