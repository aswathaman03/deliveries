package com.ash.deliveries.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity
data class Delivery(@PrimaryKey val id: Int, val description: String, val imageUrl: String, val location: Location) :
    Parcelable
