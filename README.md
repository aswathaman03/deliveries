For the google maps in details page to work, Please replace your api_key at "app/src/debug/res/values/google_maps_api.xml"

To get an api_key and to enable google maps api, follow the below steps : 

1.Go to google cloud console
2.Create a project
3.Click on hamburger menu and go to credentials.
4.Click on create api key
5.Copy the created api key "app/src/debug/res/values/google_maps_api.xml"
6.Go to dashboard, click on Enable api's at the top.
7.Enable Maps sdk for android from the list of api's provided.